export default [
  { label: 'Survey Title', name: 'title', placeholder: 'title' },
  { label: 'Subject Line', name: 'subject', placeholder: 'subject' },
  { label: 'Email Body', name: 'body', placeholder: 'email@gmail.com' },
  {
    label: 'Recipient List',
    name: 'recipients',
    placeholder: 'email1@gmail.com, email2@gmail.com',
  },
];
